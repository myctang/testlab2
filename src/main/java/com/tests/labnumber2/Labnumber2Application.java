package com.tests.labnumber2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Labnumber2Application {

    public static void main(String[] args) {
        SpringApplication.run(Labnumber2Application.class, args);
    }

}
