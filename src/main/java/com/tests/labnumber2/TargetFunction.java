package com.tests.labnumber2;

import com.tests.labnumber2.functions.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TargetFunction implements Function {
    private final Csc csc;
    private final Sec sec;
    private final Cot cot;
    private final Sin sin;
    private final Log log;
    private final Ln ln;

    public double calculate(double x, double precision) {
        if (x > 0) {
            return logarithmic(x, precision);
        } else {
            return trigonometric(x, precision);
        }
    }

    private double trigonometric(double x, double precision) {
        double partOne = (csc.csc(x, precision) - sec.sec(x, precision)) / csc.csc(x, precision);
        double partTwo = Math.pow(Math.pow(cot.cot(x, precision), 3), 3);
        double partThree = cot.cot(x, precision) + sin.sin(x, precision);
        return Math.pow(partOne - partTwo, 2) * partThree;
    }

    private double logarithmic(double x, double precision) {
        double partOne = (log.log(x, 10, precision) / log.log(x, 3, precision)) * log.log(x, 3, precision) *
                log.log(x, 3, precision) + ln.ln(x, precision);
        double partTwo = Math.pow(log.log(x, 5, precision) + log.log(x, 10, precision), 3);
        return partOne * partTwo;
    }

    public static void main(String[] args) {
        Sin sin = new Sin();
        Cos cos = new Cos(sin);
        Ln ln = new Ln();
        TargetFunction targetFunction = new TargetFunction(new Csc(sin), new Sec(cos), new Cot(cos, sin), sin, new Log(ln), ln);
        targetFunction.printCsv(0.01, -1, 50);
    }
}
