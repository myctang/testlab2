package com.tests.labnumber2.functions;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Csc implements Function {
    private final Sin sin;

    public double csc(double x, double precision) {
        if (Math.abs(x) < precision) return 0;
        return 1 / sin.sin(x, precision);
    }

    @Override
    public double calculate(double x, double precision) {
        return csc(x, precision);
    }
}
