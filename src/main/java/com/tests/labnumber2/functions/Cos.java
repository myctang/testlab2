package com.tests.labnumber2.functions;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Cos implements Function {
    private final Sin sin;

    public double cos(double x, double precise) {
        return Math.sqrt(1 - Math.pow(sin.sin(x, precise), 2));
    }

    @Override
    public double calculate(double x, double precision) {
        return cos(x, precision);
    }
}
