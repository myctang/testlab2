package com.tests.labnumber2.functions;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static java.math.BigDecimal.*;
import static java.math.BigDecimal.valueOf;

@Component
public class Ln implements Function {
    private final static BigDecimal TWO = valueOf(2);
    private final int MAX_STEPS = 1000;

    public double ln(double x, double precision) {
        BigDecimal bgx = valueOf(x);
        BigDecimal bigDecimalPrecision = valueOf(precision);
        BigDecimal y = bgx.subtract(ONE).divide(bgx.add(ONE), 10, ROUND_HALF_EVEN);
        BigDecimal result = y.multiply(TWO);
        for (int i = 0; i < MAX_STEPS; i++) {
            BigDecimal diff = y.pow(3 + i * 2).divide(valueOf(3 + i * 2), 10, ROUND_HALF_EVEN).multiply(TWO);
            if (diff.abs().compareTo(bigDecimalPrecision) < 0) {
                return result.doubleValue();
            }
            result = result.add(diff);
        }
        System.out.println("Max steps");
        return result.doubleValue();
    }

    @Override
    public double calculate(double x, double precision) {
        return ln(x, precision);
    }
}
