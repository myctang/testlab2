package com.tests.labnumber2.functions;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Sec implements Function {
    private final Cos cos;

    public double sec(double x, double precision) {
        return 1 / cos.cos(x, precision);
    }

    @Override
    public double calculate(double x, double precision) {
        return sec(x, precision);
    }
}
