package com.tests.labnumber2.functions;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Log implements Function {
    private final Ln ln;

    public double log(double x, double n, double precision) {
        return ln.ln(x, precision) / ln.ln(n, precision);
    }

    @Override
    public double calculate(double x, double precision) {
        return log(x, 10, precision);
    }
}
