package com.tests.labnumber2.functions;

import com.google.common.math.BigIntegerMath;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;

@Component
public class Sin implements Function {
    private final int MAX_STEPS = 1000;

    public double sin(double x, double precise) {
        BigDecimal y = valueOf(x);
        BigDecimal initialX = y;
        BigDecimal bigDecimalPrecise = valueOf(precise);
        for (int i = 0; i < MAX_STEPS; i++) {
            BigDecimal step = initialX.pow(3 + i * 2).divide(new BigDecimal(BigIntegerMath.factorial(3 + i * 2)), BigDecimal.ROUND_HALF_EVEN);
            if (i % 2 == 0) {
                step = step.negate();
            }
            if (step.abs().compareTo(bigDecimalPrecise) < 0) {
                return y.doubleValue();
            }
            y = y.add(step);
        }
        return y.doubleValue();
    }

    @Override
    public double calculate(double x, double precision) {
        return sin(x, precision);
    }
}
