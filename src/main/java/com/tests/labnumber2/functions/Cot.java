package com.tests.labnumber2.functions;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Cot implements Function{
    private final Cos cos;
    private final Sin sin;

    public double cot(double x, double precise) {
        return cos.cos(x, precise) / sin.sin(x, precise);
    }

    @Override
    public double calculate(double x, double precision) {
        return cot(x, precision);
    }
}
