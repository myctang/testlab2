package com.tests.labnumber2.functions;

public interface Function {
    double calculate(double x, double precision);

    default void printCsv(double step, double from, double to) {
        for (; from < to; from += step) {
            System.out.println(from + "," + calculate(from, 0.00001));
        }
    }
}
